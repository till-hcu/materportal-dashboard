import {generateSimpleGetters} from "../../../../app-store/utils/generators";
import initialState from "./stateGenericCockpit";

export default {
    ...generateSimpleGetters(initialState)
};
