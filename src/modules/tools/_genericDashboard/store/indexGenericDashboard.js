import actions from "./actionsGenericCockpit";
import mutations from "./mutationsGenericCockpit";
import getters from "./gettersGenericCockpit";
import state from "./stateGenericCockpit";

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
