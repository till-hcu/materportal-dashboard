export default {
    active: false,
    id: "genericCockpit",
    // defaults for config.json tool parameters
    name: "common:menu.tools.genericCockpit",
    glyphicon: "glyphicon-stats",
    renderToWindow: false,
    resizableWindow: true,
    isVisibleInMenu: true,
    deactivateGFI: false,
    simpleMap: false,
    // genericDashboard state
    hashes: {
        charts: [],
        filters: []
    }
};
