import {generateSimpleMutations} from "../../../../app-store/utils/generators";
import initialState from "./stateGenericCockpit";

export default {
    ...generateSimpleMutations(initialState)
};
