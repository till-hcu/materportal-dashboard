
import {generateSimpleGetters} from ".../../../src/app-store/utils/generators";
import stateGenericDashboard from "./stateGenericDashboard";

const getters = {
    ...generateSimpleGetters(stateGenericDashboard)
};

export default getters;
