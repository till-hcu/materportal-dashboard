import actions from "./actionsGenericDashboard";
import mutations from "./mutationsGenericDashboard";
import getters from "./gettersGenericDashboard";

export default {
    actions: Object.keys(actions),
    mutations: Object.keys(mutations),
    getters: Object.keys(getters)
};
