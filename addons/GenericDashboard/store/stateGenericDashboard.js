/**
 * User type definition
 * @typedef {object} GenericDashboardState
 * @property {boolean} active if true, GenericDashboard will rendered
 * @property {string} id id of the GenericDashboard component
 * @property {string} name displayed as title (config-param)
 * @property {string} glyphicon icon next to title (config-param)
 * @property {boolean} renderToWindow if true, tool is rendered in a window, else in sidebar (config-param)
 * @property {boolean} resizableWindow if true, window is resizable (config-param)
 * @property {boolean} isVisibleInMenu if true, tool is selectable in menu (config-param)
 * @property {boolean} deactivateGFI flag if tool should deactivate gfi (config-param)
 */
export default {
    active: false,
    id: "GenericDashboard",
    // defaults for config.json tool parameters
    name: "additional:modules.tools.GenericDashboard",
    glyphicon: "glyphicon-stats",
    renderToWindow: false,
    resizableWindow: true,
    isVisibleInMenu: true,
    deactivateGFI: false,
    simpleMap: false,
    initialWidth: 0.5,
    // genericDashboard state
    url: "http://localhost:5000",
    description: null,
    hashes: {
        charts: [],
        filters: []
    },
    filters: [],
    charts: [],
    additionalOptions: {
        filters: []
    }
};

