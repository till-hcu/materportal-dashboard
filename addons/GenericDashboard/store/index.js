import getters from "./gettersGenericDashboard";
import mutations from "./mutationsGenericDashboard";
import state from "./stateGenericDashboard";
import actions from "./actionsGenericDashboard";

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
};
