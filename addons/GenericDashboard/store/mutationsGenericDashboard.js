import {generateSimpleMutations} from "../../../src/app-store/utils/generators";
import stateGenericDashboard from "./stateGenericDashboard";

const mutations = {
    /**
     * Creates from every state-key a setter.
     * For example, given a state object {key: value}, an object
     * {setKey:   (state, payload) => *   state[key] = payload * }
     * will be returned.
     */
    ...generateSimpleMutations(stateGenericDashboard),
    addChart (state) {
        state.charts.push({...state.chart});
    },
    updateChart (state, index) {
        state.charts[index] = state.chart;
    },
    rmChart (state, index) {
        state.charts = [...state.charts.slice(0, index), ...state.charts.slice(index + 1, state.charts.length)];
    },
    updateFilters (state, filters) {
        state.additionalOptions = {
            ...state.additionalOptions,
            filters
        };
    },

    /**
     * If name from config.json starts with "translate#", the corrected key is set to name here.
     * @param {object} state of this component
     * @param {string} payload name of this component
     * @returns {void}
     */
    applyTranslationKey: (state, payload) => {
        if (payload && payload.indexOf("translate#") > -1) {
            state.name = payload.substr("translate#".length);
        }
    }
};

export default mutations;
