import {getResults, getJSON} from "../utils/requests";
import {composeChart} from "../utils/charts";

export default {
    async fetchData ({commit, state}) {
        const url = state.url,
            filterHashes = state.hashes.filters,
            chartHashes = state.hashes.charts,
            filterConfigs = [],
            chartConfigs = [];
        let json;

        for (const hash of chartHashes) {
            json = await getResults(url, hash);
            if (json) {
                chartConfigs.push({
                    ...composeChart(json.aggregation, json.chart),
                    hash
                });
            }
        }

        for (const hash of filterHashes) {
            json = await getJSON(url, hash, "FILTERS");
            if (json) {
                filterConfigs.push({
                    ...json,
                    hash
                });
            }
        }

        commit("setCharts", chartConfigs);
        commit("setFilters", filterConfigs);
    }
};
