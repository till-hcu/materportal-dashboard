import {expect} from "chai";
import getters from "../../../store/gettersGenericDashboard";
import stateGenericDashboard from "../../../store/stateGenericDashboard";


const {
    active,
    id,
    name,
    glyphicon,
    renderToWindow,
    resizableWindow,
    isVisibleInMenu,
    deactivateGFI} = getters;

describe("addons/GenericDashboard/store/gettersVueAddon", function () {
    it("returns the active from state", function () {
        expect(active(stateGenericDashboard)).to.be.false;
    });
    it("returns the id from state", function () {
        expect(id(stateGenericDashboard)).to.equals("GenericDashboard");
    });

    describe("testing default values", function () {
        it("returns the name default value from state", function () {
            expect(name(stateGenericDashboard)).to.be.equals("Simple Vue Addon");
        });
        it("returns the glyphicon default value from state", function () {
            expect(glyphicon(stateGenericDashboard)).to.equals("glyphicon-screenshot");
        });
        it("returns the renderToWindow default value from state", function () {
            expect(renderToWindow(stateGenericDashboard)).to.be.true;
        });
        it("returns the resizableWindow default value from state", function () {
            expect(resizableWindow(stateGenericDashboard)).to.be.true;
        });
        it("returns the isVisibleInMenu default value from state", function () {
            expect(isVisibleInMenu(stateGenericDashboard)).to.be.true;
        });
        it("returns the deactivateGFI default value from state", function () {
            expect(deactivateGFI(stateGenericDashboard)).to.be.true;
        });

    });
});
