import Vuex from "vuex";
import {config, shallowMount, createLocalVue} from "@vue/test-utils";
import GenericDashboardComponent from "../../../components/GenericDashboard.vue";
import GenericDashboard from "../../../store/index";
import {expect} from "chai";

const localVue = createLocalVue();

localVue.use(Vuex);
config.mocks.$t = key => key;

describe("addons/VueAddon/components/GenericDashboard.vue", () => {
    const mockConfigJson = {
        Portalconfig: {
            menu: {
                tools: {
                    children: {
                        VueAddon:
                            {
                                "name": "translate#additional:modules.tools.GenericDashboard.title",
                                "glyphicon": "glyphicon-th-list"
                            }
                    }
                }
            }
        }
    };
    let store;

    beforeEach(() => {
        store = new Vuex.Store({
            namespaces: true,
            modules: {
                Tools: {
                    namespaced: true,
                    modules: {
                        GenericDashboard
                    }
                }
            },
            state: {
                configJson: mockConfigJson
            }
        });
        store.commit("Tools/GenericDashboard/setActive", true);
    });

    it("renders the VueAddon", () => {
        const wrapper = shallowMount(GenericDashboardComponent, {store, localVue});

        expect(wrapper.find("#generic-dashboard").exists()).to.be.true;
    });

    it("do not render the GenericDashboard if not active", () => {
        let wrapper = null;

        store.commit("Tools/GenericDashboard/setActive", false);
        wrapper = shallowMount(GenericDashboardComponent, {store, localVue});

        expect(wrapper.find("#generic-dashboard").exists()).to.be.false;
    });
    it("GenericDashboard contains correct html", () => {
        const wrapper = shallowMount(GenericDashboardComponent, {store, localVue});

        expect(wrapper.find("#generic-dashboard").html()).to.be.equals("<div id=\"generic-dashboard\">\n  additional:modules.tools.GenericDashboard.content\n</div>");
    });

});
