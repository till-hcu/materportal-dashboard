import GenericDashboardComponent from "./components/GenericDashboard.vue";
import GenericDashboardStore from "./store/index";
import deLocale from "./locales/de/additional.json";
import enLocale from "./locales/en/additional.json";

export default {
    component: GenericDashboardComponent,
    store: GenericDashboardStore,
    locales: {
        de: deLocale,
        en: enLocale
    }
};
