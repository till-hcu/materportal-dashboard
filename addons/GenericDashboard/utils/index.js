export default {
    getRandomColor (clamp = [100, 255], alpha = 1.0) {
        const r = Math.round((Math.random() * (clamp[1] - clamp[0])) + clamp[0]),
            g = Math.round((Math.random() * (clamp[1] - clamp[0])) + clamp[0]),
            b = Math.round((Math.random() * (clamp[1] - clamp[0])) + clamp[0]);

        return `rgba(${r}, ${g}, ${b}, ${alpha})`;
    },
    sanitizeChartOptions (chartOptions) {
        const _chartOptions = {};

        for (const opt in chartOptions) {
            if (chartOptions[opt].constructor === Object) {
                _chartOptions[opt] = this.sanitizeChartOptions(chartOptions[opt]);
            }
            else if (Array.isArray(chartOptions[opt]) && chartOptions[opt].length > 0) {
                _chartOptions[opt] = chartOptions[opt].map(el => isNaN(parseFloat(el)) ? el : parseFloat(el));
                if (_chartOptions[opt].length === 1) {
                    _chartOptions[opt] = _chartOptions[opt][0];
                }
            }
            else if (!isNaN(parseFloat(chartOptions[opt]))) {
                _chartOptions[opt] = parseFloat(chartOptions[opt]);
            }
            else {
                _chartOptions[opt] = chartOptions[opt];
            }
        }
        return _chartOptions;
    }
};
