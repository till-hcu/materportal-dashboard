/* eslint-disable require-jsdoc */
import Utils from "./index.js";

let _backgroundColors;

function resetTempValues () {
    _backgroundColors = undefined;
}

function getXLabels (result, separator = "/") {
    const indices = result.GroupByAttributes.length;

    return result.AggregationResults.map(el => el.slice(0, indices).join(separator));
}

function getYLabel (label, result, separator = "/") {
    return result.AggregationAttribute + separator + label;
}

function getBarDataStyles (data, datasetCount, datasetIndex = 0, customColors = []) {
    if (datasetCount === 1) {
        return data.map((v, i) => {
            if (customColors.length === 1) {
                return customColors[0];
            }
            if (customColors.constructor === Object) {
                return customColors[v] || Utils.getRandomColor();
            }
            return customColors[i] || Utils.getRandomColor();
        });
    }
    return customColors[datasetIndex] || Utils.getRandomColor();
}

function getPieDataStyles (data, datasetCount, datasetIndex = 0, customColors = []) {
    if (datasetIndex) {
        // .. some things that might happen with the datasetIndex
    }

    const backgroundColors = _backgroundColors || data.map((v, i) => {
        if (customColors.constructor === Object) {
            return customColors[v] || Utils.getRandomColor();
        }
        return customColors[i] || Utils.getRandomColor();
    });

    _backgroundColors = backgroundColors;

    return backgroundColors;
}

function getDataStyles (data, datasetCount, datasetIndex, chartType, opts) {
    let backgroundColor = [];

    switch (chartType) {
        case "bar":
            backgroundColor = getBarDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor);
            break;
        case "line":
            backgroundColor = getBarDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor);
            break;
        case "pie":
            backgroundColor = getPieDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor);
            break;
        case "polarArea":
            backgroundColor = getPieDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor);
            break;
        default:
            backgroundColor = opts.backgroundColor;
    }

    return {
        ...opts,
        backgroundColor
    };
}

function getDatasets (result, chartType = "bar", datasetOptions = {}) {
    const opts = datasetOptions,
        cullIndices = result.GroupByAttributes.length;

    return result.AggregationFunctions.map((_label, i) => {
        const label = getYLabel(_label, result),
            data = result.AggregationResults.map(el => el[cullIndices + i]);

        return {
            label,
            data,
            ...getDataStyles(data, result.AggregationFunctions.length, i, chartType, opts)
        };
    });
}

export function composeChart (result, config) {
    const chartType = config.chartType,
        chartOptions = config.chartOptions,
        chartData = {
            labels: getXLabels(result),
            datasets: getDatasets(result, chartType, chartOptions.dataset)
        };

    resetTempValues();

    return {
        chartType,
        chartData,
        chartOptions
    };
}
