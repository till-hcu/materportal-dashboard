/* eslint-disable require-jsdoc */
function parseResult (str) {
    let json;

    try {
        json = JSON.parse(str);
    }
    catch (e) {
        console.warn("could not fetch config from backend", e);
    }

    return json;
}

export async function getResults (url, hash, additionalOptions = {}) {
    let uri = url + "/getResults";

    uri += `?hash=${hash}`;
    const response = await fetch(uri, {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(additionalOptions)
        }),
        result = await response.text();

    return parseResult(result);
}

export async function getJSON (url, hash, table) {
    let uri = url + "/getJSON";

    uri += `?hash=${hash}`;
    uri += `&table=${table}`;
    const response = await fetch(uri),
        result = await response.text();

    return parseResult(result);
}
